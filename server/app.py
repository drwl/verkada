import time
from flask import Flask, jsonify, request
import redis

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379, decode_responses=True)

# Max amount of time (in seconds) to keep connection alive before returning a response
CAMERA_CONNECTION_TIMEOUT = 60

# Max amount of time (in seconds) to gather camera logs before returning
LOG_RETURN_TIMEOUT = 30

# Interval of delay (in seconds)
TIMEOUT_DELAY = 0.5

def try_redis(method, *args):
    """
    A helper method to execute redis commands that retries up to 5 times before
    throwing an error. Useful if redis server is down or isn't responsive.
    """
    retries = 5
    while True:
        try:
            return getattr(cache, method)(*args)
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

def create_default_response_object():
    """
    Factory method for creating a default response dictionary that gets returned
    for each request.
    """
    response = {'status_msg': ''}
    return response

def connected_cameras_count():
    """
    A helper method to return the number of connected cameras.
    """
    return try_redis('scard', 'cameras_connected')

@app.route('/count/')
def count_cameras():
    """
    Responds with the number of cameras connected.
    """
    response = create_default_response_object()
    response['status_msg'] = 'There are currenty %d cameras connected.' % connected_cameras_count()
    return jsonify(response), 200

def return_logs():
    """
    A helper method that gathers logs from Redis and returns a modified response.
    """
    response = create_default_response_object()
    logs = try_redis('hgetall', 'logs')
    response['camera_count'] = len(logs)
    response['camera_logs'] = logs
    try_redis('delete', 'logs')
    return response

@app.route('/logs/')
def get_logs():
    """ TODO """

    if connected_cameras_count():
        try_redis('sunionstore', 'cameras_require_logs', 'cameras_connected')
        time_wait = LOG_RETURN_TIMEOUT
        while try_redis('scard', 'cameras_require_logs') and time_wait > 0:
            pending_cameras = try_redis('scard', 'cameras_require_logs')
            finished_cameras = try_redis('hlen', 'logs')
            app.logger.info('Waiting to receive logs from %s cameras. Time remaining: %ds',
                pending_cameras, time_wait)
            time_wait -= TIMEOUT_DELAY
            time.sleep(TIMEOUT_DELAY)
        response = return_logs()
        if try_redis('scard', 'cameras_require_logs'):
            app.logger.info('Got logs from %d cameras.', finished_cameras)
            response['status_msg'] = 'Returning partial logs.'
        else:
            app.logger.info('Got logs from all cameras.')
            response['status_msg'] = 'Returning logs from all connected cameras.'
        return jsonify(response), 200
    else:
        response = create_default_response_object()
        response['status_msg'] = 'No connected cameras.'
        return jsonify(response), 200

@app.route('/upload/<int:camera_id>', methods=['POST'])
def upload_logs(camera_id):
    """ TODO """
    response = create_default_response_object()
    if try_redis('sismember', 'cameras_require_logs', camera_id):
        req = request.get_json()
        app.logger.info('Got upload from camera %d', camera_id)
        try_redis('hset', 'logs', camera_id, req['logs'])
        try_redis('srem', 'cameras_require_logs', camera_id)
        response['status_msg'] = 'Got logs.'
        return jsonify(response), 200
    else:
        app.logger.info('Got upload from camera %d, but did not want logs or already have them.', camera_id)
        response['status_msg'] = 'Got logs but did not want them.'
        return jsonify(response), 204

@app.route('/connect/<int:camera_id>')
def connect_camera(camera_id):
    """ TODO """
    app.logger.info('Connection established with camera %d', camera_id)
    try_redis('sadd', 'cameras_connected', camera_id)
    response = create_default_response_object()
    response['action'] = ''
    time_wait = CAMERA_CONNECTION_TIMEOUT
    while time_wait > 0:
        if try_redis('sismember', 'cameras_require_logs', camera_id):
            app.logger.info('Telling camera %d to send logs', camera_id)
            try_redis('srem', 'cameras_connected', camera_id)
            response['action'] = 'send'
            return jsonify(response), 200
        time_wait -= TIMEOUT_DELAY
        time.sleep(TIMEOUT_DELAY)
    try_redis('srem', 'cameras_connected', camera_id)
    return jsonify(response), 204

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, threaded=True)
