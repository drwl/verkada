import aiohttp
import asyncio
import concurrent.futures
import json
import logging
import random

class ListHandler(logging.Handler):
    def __init__(self, *args, list, **kwargs):
        logging.Handler.__init__(self, *args, **kwargs)
        self.list = list

    def emit(self, record):
        self.list.append(self.format(record).rstrip('\n'))

class Camera:
    """
    A virtual camera used to simulate real Verkada cameras.
    """
    # The following times are in seconds
    ACTIVITY_DELAY = 10
    CONNECTION_TIMEOUT = 60

    camera_activities = [
        'User started viewing live video',
        'Changing aperture +10',
        'Changing aperture -10',
        'Physical activity detected',
        'User changed zoom +10',
        'User changed zoom -10'
    ]

    def __init__(self, id, server):
        self.id = id
        self.server = server
        self.activity = []
        self.logger = self.setup_logger()
        self.retry_delay = 1 # Timer delay for when camera is unable to connect to server

    def start(self):
        loop = asyncio.get_event_loop()
        asyncio.ensure_future(self.generate_activity())
        asyncio.ensure_future(self.connect())

        try:
            loop.run_forever()
        except KeyboardInterrupt:
            for task in asyncio.Task.all_tasks():
                task.cancel()
            self.logger.debug('Received command to stop camera by user.')
        finally:
            self.logger.debug('Camera stopped. Restart script to resume virtual camera.')

    def connect_path(self):
        return '%s/connect/%d' % (self.server, self.id)

    def upload_path(self):
        return '%s/upload/%d' % (self.server, self.id)

    async def connect(self):
        async with aiohttp.ClientSession() as session:
            try:
                self.logger.debug('Creating new connection with server ...')
                path = self.connect_path()
                async with session.get(path, timeout=self.CONNECTION_TIMEOUT) as response:
                    result = await response.json()
                    if result['action'] == 'send':
                        self.logger.debug('Server is requesting camera logs.')
                        await self.send_logs()
                    else:
                        self.logger.debug('No response from server.')
            except concurrent.futures.TimeoutError:
                self.logger.debug('Timed out after %d seconds.', self.CONNECTION_TIMEOUT)
            except aiohttp.client_exceptions.ClientConnectorError:
                self.logger.debug('Unable to connect to server. Waiting %d seconds before trying to reconnect.',
                    self.retry_delay)
                await asyncio.sleep(self.retry_delay)
                self.retry_delay *= 2
            finally:
                asyncio.ensure_future(self.connect())

    async def generate_activity(self):
        while True:
            self.logger.info(random.choice(self.camera_activities))
            await asyncio.sleep(self.ACTIVITY_DELAY)

    async def send_logs(self):
        self.logger.debug('Sending logs to server.')
        async with aiohttp.ClientSession() as session:
            path = self.upload_path()
            async with session.post(path, json={'logs': self.activity}) as response:
                result = await response.json()
                self.logger.debug('Posted logs to server. Got back response: %s', result['status_msg'])

    def setup_logger(self):
        def logging_filter(record):
            """
            A hacky solution to ensure that only fake camera activity is processed by List Handler.
            """
            return record.levelno == logging.INFO

        logger = logging.getLogger()
        logger.setLevel(logging.DEBUG)

        console_handler = logging.StreamHandler()
        formatter = logging.Formatter(fmt='%(asctime)s %(levelname)s - %(message)s')
        console_handler.setFormatter(formatter)
        console_handler.setLevel(logging.DEBUG)
        logger.addHandler(console_handler)

        camera_log_handler = ListHandler(list=self.activity, level=logging.INFO)
        formatter = logging.Formatter(fmt='%(asctime)s - %(message)s')
        camera_log_handler.addFilter(logging_filter)
        camera_log_handler.setFormatter(formatter)
        logger.addHandler(camera_log_handler)

        return logger

if __name__ == '__main__':
    camera = Camera(2, 'http://localhost:5000')
    camera.start()
