import os
from multiprocessing import Process
from camera import Camera

def start_camera(id, url):
    camera = Camera(id, url)
    camera.start()

if __name__ == '__main__':
    server_url = 'http://web:5000'

    num_cameras = int(os.getenv('NUM_CAMERAS', 2))
    cameras = []

    for i in range(num_cameras):
        cam = Process(target=start_camera, args=(i, server_url))
        cameras.append(cam)
        cam.start()
