# Overview

### How to use
- Make sure you are in the root directory
- Build the containers by running `docker-compose build`  
- Start the entire project by running `docker-compose up`.  
You should see a lot of console output with logs from each container.
- To shutdown the project run `docker-compose down`.

### Server
- Is a bare bones Flask API server that returns JSON responses.
- `http://localhost:5000/logs/`
- `http://localhost:5000/count/`

### Support for N cameras
Change number of cameras by changing line 19 in `docker-compose.yml`. By default it is set to `NUM_CAMERAS: 4`.

### About
- This example client-server project uses Flask 1.0, Redis, and Python 3.6+.
- The server is designed for availability and does not guarantee consistency. When returning logs to user it will attempt
to wait for all connected cameras to send logs. After a set amount of time the server will return only the logs it holds. The timeout is modifiable in `server/app.py` by changing `LOG_RETURN_TIMEOUT = 30`.
- It is designed for only one user to request /logs/ at a time.

### Lessons Learned
- A lot more experience with Python 3
- Learned about co-routines in Python and how it's changed throughout Python 3 from 3.4 to 3.5+
- Using Redis

### Future Considerations
- Security:
  - Flask Server and Redis instance would be in a private VPC
  - Use application load balancer to distribute connections to cameras
  - Authentication to view logs
- Use an ORM for easier abstraction with datastore/database
- Use an app server
- Depending on logs usage, save them to a datastore
- Compress logs or allow for a subset of logs to be requested by server
